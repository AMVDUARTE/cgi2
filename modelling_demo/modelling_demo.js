var gl;

var canvas;

// GLSL programs
var program;

//Ration 1 -> 0.625
// Render Mode
var WIREFRAME=1;
var FILLED=2;
var renderMode = WIREFRAME;
var FLOOR = -0;

var gamma = 42;
var theta = 7;

var projection;
var modelView;
var view;
var up= 0;
var rot= 0;
var d= 0;

matrixStack = [];

function pushMatrix()
{
    matrixStack.push(mat4(modelView[0], modelView[1], modelView[2], modelView[3]));
}

function popMatrix() 
{
    modelView = matrixStack.pop();
}

function multTranslation(t) {
    modelView = mult(modelView, translate(t));
}

function multRotX(angle) {
    modelView = mult(modelView, rotateX(angle));
}

function multRotY(angle) {
    modelView = mult(modelView, rotateY(angle));
}

function multRotZ(angle) {
    modelView = mult(modelView, rotateZ(angle));
}

function multMatrix(m) {
    modelView = mult(modelView, m);
}
function multScale(s) {
    modelView = mult(modelView, scalem(s));
}

function initialize() {
    gl.clearColor(0.0, 0.0, 0.0, 1.0);
    gl.viewport(0,0,canvas.width, canvas.height);
    gl.enable(gl.DEPTH_TEST);
    
    program = initShaders(gl, "vertex-shader-2", "fragment-shader-2");
    
    cubeInit(gl);
    sphereInit(gl);
    cylinderInit(gl);

    setupView();
    setupProjection();
}

function setupProjection() {
	theta =  document.getElementById("theta").value;
	gamma = document.getElementById("gamma").value;

	modelView = mult(modelView,mult(rotateX(gamma),rotateY(theta)));
    //projection = perspective(60, 1, 0.1, 100);
    projection = ortho(-20,20,-20,20,-25,15);
}

function setupView() {
    view = lookAt([0,0,5], [0,0,0], [0,1,0]);
    modelView = mat4(view[0], view[1], view[2], view[3]);
}

function setMaterialColor(color) {
    var uColor = gl.getUniformLocation(program, "color");
    gl.uniform3fv(uColor, color);
}

function sendMatrices()
{
    // Send the current model view matrix
    var mView = gl.getUniformLocation(program, "mView");
    gl.uniformMatrix4fv(mView, false, flatten(view));
    
    // Send the normals transformation matrix
    var mViewVectors = gl.getUniformLocation(program, "mViewVectors");
    gl.uniformMatrix4fv(mViewVectors, false, flatten(normalMatrix(view, false)));  

    // Send the current model view matrix
    var mModelView = gl.getUniformLocation(program, "mModelView");
    gl.uniformMatrix4fv(mModelView, false, flatten(modelView));
    
    // Send the normals transformation matrix
    var mNormals = gl.getUniformLocation(program, "mNormals");
    gl.uniformMatrix4fv(mNormals, false, flatten(normalMatrix(modelView, false)));  
}

function draw_sphere(color)
{
    setMaterialColor(color);
    sendMatrices();
    sphereDrawFilled(gl, program);
}

function draw_cube(color)
{
    setMaterialColor(color);
    sendMatrices();
    cubeDrawFilled(gl, program);
}

function draw_cylinder(color)
{
    setMaterialColor(color);
    sendMatrices();
    cylinderDrawFilled(gl, program);
}

function draw_scene2()
{
    
    var d = (new Date()).getTime();
    
    // Up and Down multTranslation([0,Math.sin(d/500),0]);
    pushMatrix();
        multTranslation([0,1,0]);
        multRotZ(d/5);
        draw_cube([0,0,1]);
    popMatrix();
    pushMatrix();
        // In a circle (left) multRotY((d/100));
        pushMatrix();
            multTranslation([-2.0,0, 0.0]);
            draw_sphere([1.0, 0.0, 0.0]);
        popMatrix();
        pushMatrix();
            pushMatrix();
                multTranslation([2,0,0]);
                draw_cylinder([0.0, 1.0, 0.0]);
            popMatrix();
        popMatrix();
    
    pushMatrix();
        pushMatrix();
            draw_cylinder([0.0, 1.0, 0.0]);
        popMatrix();
        pushMatrix();
        multTranslation([1,0,0]);
            draw_sphere([0.0, 1.0, 1.0]);
        popMatrix();
    popMatrix();
    popMatrix();
}

function draw_Floor(){
    pushMatrix();
    multTranslation([0,FLOOR -0.9,0]);
    multScale([20.0,0.5,20.0]);
    draw_cube([1.0, 1.0, 0.0]);
    popMatrix();
}

function draw_scene(){
   pushMatrix();
    draw_Floor();
    multRotY(rot*10); 
    multTranslation([9.375,up,0]); 
    multRotY(90); //Looks at place where it's turning
    multScale([0.5, 0.5, 0.5]);
    pushMatrix();
        multTranslation([-1,1,0]);
        multScale([0.75,0.75,0.75]);
        draw_Helice();
    popMatrix();
    pushMatrix();
        multTranslation([2.4,0.8,0.4]);
        multRotX(90);
        multScale([0.3,0.3,0.3]);
        draw_Helice();
    popMatrix();
    pushMatrix();
        draw_Body();
    popMatrix();
    pushMatrix();
        multTranslation([-1,-0.5,0]);
        multScale([0.75,0.75,0.75]);
        draw_leg();
    popMatrix();
    popMatrix();
}

function draw_Helice(){
    d = (new Date()).getTime() * 10;
	pushMatrix();
	    multScale([0.5,1.0,0.5]);
	    draw_cylinder([0.0, 1.0, 0.0]);
	popMatrix();
    pushMatrix();
	    multRotY((d/5));
	    multScale([2.0,0.4,1.0]);
        pushMatrix();
            multTranslation([-0.5,0.5,0]);
            draw_sphere([0.0, 1.0, 1.0]);
        popMatrix();
        pushMatrix();
            multTranslation([0.5,0.5,0]);
            draw_sphere([0.0, 1.0, 1.0]);
        popMatrix();
    popMatrix();
   
}
function draw_Body(){
    pushMatrix();
        pushMatrix();
            multTranslation([-1,0,0]);
            multScale([3, 1.5, 1.5]);
            draw_sphere([1.0, 0.0, 0.0]);
        popMatrix();
     	pushMatrix();
	        multTranslation([1,0.4,0]);
	        multScale([3.0, 0.5, 0.5]);
	        draw_sphere([1.0, 0.0, 0.0]);
	    popMatrix();
    	pushMatrix();
            multTranslation([2.5,0.7,0]);  
            multRotX(45);
            multScale([0.5, 1, 0.7]);
            draw_sphere([1.0, 0.0, 0.0]);
        popMatrix();
    popMatrix();
}

function draw_leg(){
    pushMatrix();
    	pushMatrix();
        	multTranslation([0,0,0.55]);
			pushMatrix();
				multTranslation([0,-0.9,0.2]);
				multRotZ(90);
				multScale([0.2, 3, 0.2]);
				draw_cylinder([0.0, 1.0, 0.0]);
			popMatrix();
			pushMatrix();
				multTranslation([0.75,-0.5,0]);
				multRotZ(30);
				multRotX(-20);
				multScale([0.2, 1, 0.2]);
				draw_cube([0,0,1]);
			popMatrix();
			pushMatrix();
				multTranslation([-0.75,-0.5,0]);
				multRotZ(-30);
				multRotX(-20);
				multScale([0.2, 1, 0.2]);
				draw_cube([0,0,1]);
			popMatrix();
    	popMatrix();
    	pushMatrix();
        	multTranslation([0,0,-0.55]);
			pushMatrix();
				multTranslation([0,-0.9,-0.2]);
				multRotZ(90);
				multScale([0.2, 3, 0.2]);
				draw_cylinder([0.0, 1.0, 0.0]);
			popMatrix();
			pushMatrix();
				multTranslation([0.75,-0.5,0]);
				multRotZ(30);
				multRotX(20);
				multScale([0.2, 1, 0.2]);
				draw_cube([0,0,1]);
			popMatrix();
			pushMatrix();
				multTranslation([-0.75,-0.5,0]);
				multRotZ(-30);
				multRotX(20);
				multScale([0.2, 1, 0.2]);
				draw_cube([0,0,1]);
			popMatrix();
    	popMatrix();
	popMatrix();
}

function render() {
    gl.clear(gl.COLOR_BUFFER_BIT | gl.DEPTH_BUFFER_BIT);

    gl.useProgram(program);
    
    setupView();
    setupProjection();
    
    // Send the current projection matrix
    var mProjection = gl.getUniformLocation(program, "mProjection");
    gl.uniformMatrix4fv(mProjection, false, flatten(projection));
        
    draw_scene();
    
    requestAnimFrame(render);
}


window.onload = function init()
{
    canvas = document.getElementById("gl-canvas");
    gl = WebGLUtils.setupWebGL(canvas);
    if(!gl) { alert("WebGL isn't available"); }
    
    initialize();
    cursor();
            
    render();
}

function cursor(){
    document.addEventListener("keydown", function(e){
        switch(e.keyCode){
            case 37:
                if(up != FLOOR){
                rot-=.1;
                rot%=36;
                console.log("going left");
                    }
                break;
            case 38:
                console.log("going up");
                up+=0.1;
                up = Math.min(2, up);
                break;
            case 40:
                console.log("going down");
                up-= 0.1;
                up = Math.max(FLOOR, up);
                break;
                }
    });
}