var gl;
var program;

var at = [0, 0, 0];
var eye = [1, 1, 1];
var up = [0, 1, 0];
var mModelView = lookAt(eye, at, up);
var mProjection = ortho(-1,1,-1,1,-10, 10);
var mNormals = transpose(inverse(mModelView));

var mModelViewLoc;
var mProjectionLoc;
var mNormalsLoc;

var l = 0.5;
var alpha = 45;
var gamma = 42;
var tetha = 7;
var d = 2;

var vtxDef = "vtx-shader.glsl";
var frgDef = "frg-shader.glsl";
var vtx = vtxDef;
var frg = frgDef;

var lSlider = {nome : "l", min : 0.0, max : 1.0, step : 0.1, value: l};
var alhpaSlider = {nome : "alpha", min : 0.0, max : 90.0, step : 1.0, value: alpha};
var gammaSlider = {nome : "gamma", min : 0.0, max : 180.0, step : 1.0, value: gamma};
var tethaSlider= {nome : "tetha", min : 0.0, max : 180.0, step : 1.0, value: tetha};
var dSlider = {nome : "d", min : 1, max : 5, step : 0.1, d: d};

var input;
var father;
var x = window.innerWidth;
var y = window.innerHeight;

var shape = "cube";
var filled = false;

var canvas;

window.onload = function init() {
	canvas = document.getElementById("gl-canvas");
	gl = WebGLUtils.setupWebGL(canvas);
	if(!gl) { alert("WebGL isn't available"); }
    // Load shaders and initialize attribute buffers
    program = initShaders(gl, vtx, frg);
    setup();
    father = document.getElementById("input");
    setupLis();
    cubeInit(gl);
    resetSettings();
    changeCanvas();
    gl.uniformMatrix4fv(mProjectionLoc, false, flatten(mProjection));
    // Configure WebGL
    gl.viewport(0,0,canvas.width, canvas.height);
    gl.clearColor(0.0, 0.0, 0.0, 0.5);
    render();   
}

function setup(){
	gl.useProgram(program);
	mModelViewLoc = gl.getUniformLocation(program, "mModelView");
	mProjectionLoc = gl.getUniformLocation(program,"mProjection");
	gl.uniformMatrix4fv(mProjectionLoc, false, flatten(mProjection));
	gl.uniformMatrix4fv(mModelViewLoc, false, flatten(mModelView));
	mNormalsLoc = gl.getUniformLocation(program, "mNormals");
	gl.uniformMatrix4fv(mNormalsLoc, false, flatten(mNormals));
}

function changeCanvas(){
	var ratio = (window.innerWidth/2) / (window.innerHeight*.95);
	if(ratio > 1)
		mProjection = ortho(-ratio, ratio, -1, 1, -10,10);
	else
		mProjection = ortho(-1, 1, -1/ratio, 1/ratio, -10,10);
	gl.uniformMatrix4fv(mProjectionLoc, false, flatten(mProjection));
    // Configure WebGL
    document.getElementById("gl-canvas").width = window.innerWidth/2;
    document.getElementById("gl-canvas").height = window.innerHeight *.95;
    gl.viewport(0,0,canvas.width, canvas.height);
    gl.clearColor(0.0, 0.0, 0.0, 0.5);
    
}

function draw(){
	mNormals = transpose(inverse(mModelView));
	gl.uniformMatrix4fv(mNormalsLoc, false, flatten(mNormals));
	gl.uniformMatrix4fv(mModelViewLoc, false, flatten(mModelView));
}

function resetSettings(){
	document.getElementById("select").value = 6;
	document.getElementById("filled").value = false;
	document.getElementById("cube").checked = "checked";
	document.getElementById("wireFrame").checked = "checked";
	document.getElementById("frg").value = frgDef;
	document.getElementById("vtx").value = vtxDef;
}

function render() {
	gl.clear(gl.COLOR_BUFFER_BIT | gl.DEPTH_BUFFER_BIT);
	switch(shape){
		case "cube":
		if(filled) cubeDrawFilled(gl, program);
		else cubeDrawWireFrame(gl, program);
		break;
		case "cylinder":
		if(filled) cylinderDrawFilled(gl, program);
		else
			cylinderDrawWireFrame(gl, program);
		break;
		case "pyramid":
		if(filled) pyramidDrawFilled(gl, program);
		else
			pyramidDrawWireFrame(gl, program);
		break;
		case "sphere":
		if(filled) sphereDrawFilled(gl, program);
		else
			sphereDrawWireFrame(gl, program);
		break;
		case "torus":
		if(filled) torusDrawFilled(gl, program);
		else
			torusDrawWireFrame(gl, program);
		break;
		case "cone":
		if(filled) coneDrawFilled(gl, program);
		else
			coneDrawWireFrame(gl, program);
		break;
	}
	requestAnimationFrame(render);
}

function setupLis(){

	document.getElementById("select").oninput = function(){
		change();
	};

    //Change shape
    document.getElementById("cube").onclick = function(){
    	cubeInit(gl);
    	shape = "cube";
    };

    document.getElementById("cylinder").onclick = function(){
    	cylinderInit(gl);
    	shape ="cylinder";  
    };
    
    document.getElementById("pyramid").onclick = function(){
    	pyramidInit(gl);
    	shape="pyramid";
    };

    document.getElementById("sphere").onclick = function(){
    	sphereInit(gl);
    	shape="sphere";
    };

    document.getElementById("torus").onclick = function(){
    	torusInit(gl);
    	shape="torus";
    };
    
    document.getElementById("cone").onclick = function(){
    	coneInit(gl);
    	shape="cone";
    };

 	//Change filled and wire
 	document.getElementById("filled").onclick = function(){
 		filled = true;
 	};

 	document.getElementById("wireFrame").onclick = function(){
 		filled = false;
 	};

    //Change shader
    document.getElementById("submitShaders").addEventListener("click", function(){
    	var vtxtemp = document.getElementById("vtx").value;
    	var frgtemp = document.getElementById("frg").value;
        try{
    		program = initShaders(gl, vtxtemp, frgtemp); 
            vtx = vtxtemp;
    		frg = frgtemp
        }catch(e){
    		vtx = vtxDef;
    		frg = frgDef;
            document.getElementById("vtx").value = vtxDef;
    		document.getElementById("frg").value = frgDef;
    		program = initShaders(gl, vtx, frg);
        }
    	setup();

    });

    document.getElementById("resetShaders").addEventListener("click", function(){
    	vtx = vtxDef;
		frg = frgDef;
		document.getElementById("vtx").value = vtxDef;
		document.getElementById("frg").value = frgDef;
		program = initShaders(gl, vtx, frg);
		setup();
    });

    //Setup Culler
    document.getElementById("zbuf").addEventListener("change", function(e){
    	if(document.getElementById("zbuf").checked)
    		gl.enable(gl.DEPTH_TEST);
    	else
    		gl.disable(gl.DEPTH_TEST);
    });

    document.getElementById("bfc").addEventListener("change", function(e){

    	if(document.getElementById("bfc").checked){
    		gl.enable(gl.CULL_FACE);
    	}
    	else{
    		gl.disable(gl.CULL_FACE);
    	}
    });

    //Change windowsize
    window.onresize = function(){
    	changeCanvas();
    }
}

function change(){
	var change = arguments[0];
	var mudar = change != undefined;
	if(mudar) switch(change){
		case "l":
		l = document.getElementById("l").value;
		lSlider.value = l;
		break;
		case "alpha":
		alpha = document.getElementById("alpha").value;
		alhpaSlider.value = alpha;
		break;
		case "d": 
		d = document.getElementById("d").value;
		dSlider.value = d;
		break;
		case "gamma":
		gamma = document.getElementById("gamma").value;
		gammaSlider.value = gamma; 
		break;
		case "tetha":
		tetha = document.getElementById("tetha").value;
		tethaSlider.value = tetha;
		break;
	}

	var perspetiva = document.getElementById("select").value;
	switch(Number(perspetiva)){
		case 0:
		mModelView =  mat4(1.0, 0.0, 0.0, 0.0,
			0.0, 1.0, 0.0, 0.0,
			0.0, 0.0, 1.0, 0.0,
			0.0, 0.0, 0.0, 1.0);
		if(!mudar)
			removeSlider();
		break;
		case 1:
		mModelView =  mult(mat4(1.0, 0.0, 0.0, 0.0,
			0.0, 1.0, 0.0, 0.0,
			0.0, 0.0, 1.0, 0.0,
			0.0, 0.0, 0.0, 1.0), rotateY(90.0));
		if(!mudar) 
			removeSlider();
		break;
		case 2:
            mModelView =  mult(mat4(1.0, 0.0, 0.0, 0.0,
			0.0, 1.0, 0.0, 0.0,
			0.0, 0.0, 1.0, 0.0,
			0.0, 0.0, 0.0, 1.0), rotateX(90.0));
		if(!mudar) 
			removeSlider();
		break;
		case 3:
		if(!mudar)
			createSlider(gammaSlider, tethaSlider);

		mModelView =  mult(mat4(1.0, 0.0, 0.0, 0.0,
			0.0, 1.0, 0.0, 0.0,
			0.0, 0.0, 1.0, 0.0,
			0.0, 0.0, 0.0, 1.0),
		mult(rotateX(gamma),rotateY(tetha)));
		break;
		case 4:
		if(!mudar) 
			createSlider(alhpaSlider, lSlider);

		mModelView =  mat4(1.0, 0.0, -l* Math.cos(radians(alpha)), 0.0,
			0.0, 1.0, -l* Math.sin(radians(alpha)), 0.0,
			0.0, 0.0, 1.0, 0.0,
			0.0, 0.0, 0.0, 1.0);
		break;
		case 5:
		if(!mudar) 
			createSlider(dSlider);

		mModelView =  mat4(1.0, 0.0, 0.0, 0.0,
			0.0, 1.0, 0.0, 0.0,
			0.0, 0.0, 1.0, 0.0,
			0.0, 0.0, -1.0/d, 1.0);
		break;
		case 6:
		mModelView = lookAt(eye, at, up);
		if(!mudar) 
			removeSlider();
		break;
	}
	draw();     
}

function removeSlider(){
	if(input!= null){
		father.removeChild(input);
		input = null;
	}
}

function createSlider(){
	if(input!=null)
		removeSlider();
	input = document.createElement("div");
	var slider;
	var text
	for(var i = 0; i < arguments.length; i++){
		text = document.createTextNode(arguments[i].nome);
		slider = document.createElement("input");
		slider.type="range";
		slider.id=arguments[i].nome;
		slider.step = arguments[i].step;
		slider.min = arguments[i].min;
		slider.max = arguments[i].max;
		slider.value = arguments[i].value;
		slider.onchange = function(){change(this.id)};
		slider.oninput = function(){change(this.id)};
		input.appendChild(text);
		input.appendChild(slider);
	}
	input.id="sliders";
	father.appendChild(input);
}