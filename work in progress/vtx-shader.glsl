attribute vec4 vPosition;
uniform mat4 mModelView;
uniform mat4 mView;
uniform mat4 mProjection;
attribute vec4 vNormal;
varying vec4 originalPosition;

void main(){
	gl_Position = mProjection * mModelView * vPosition;
	originalPosition = vPosition;
}
